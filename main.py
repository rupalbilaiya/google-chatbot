from flask import Flask, request
import pandas as pd

df = pd.read_excel(r'Adventure.xls', sheet_name='Person')
df1 = pd.read_excel(r'Adventure.xls', sheet_name='EmailAddress')

app = Flask(__name__)

@app.route('/') # this is the home page route
def hello_world(): # this is the home page function that generates the page code
    return "Hello world!"
    
@app.route('/about')
def welcome():
  return "Welcome to my Flask site, ©2022"

@app.route('/webhook', methods=['POST'])
def webhook():
  req = request.get_json(silent=True, force=True)
  fulfillmentText = ''
  sum = 0
  query_result = req.get('queryResult')
  print(query_result.get('action'))
  if query_result.get('action') == 'add.numbers':
    num1 = int(query_result.get('parameters').get('number'))
    num2 = int(query_result.get('parameters').get('number1'))
    sum = str(num1 + num2)
    fulfillmentText = 'The sum of the two numbers is '+sum
  elif query_result.get('action') == 'multiply.numbers':
    num1 = int(query_result.get('parameters').get('number'))
    num2 = int(query_result.get('parameters').get('number1'))
    product = str(num1 * num2)
    fulfillmentText = 'The product of the two numbers is '+product
  elif query_result.get('action') == 'person.sheet':
    persontype = query_result.get('parameters').get('person')
    print(persontype)
    if persontype=='':
      fulfillmentText = 'Different types of person types' + str(df.PersonType.unique())
    else:
      fulfillmentText = 'Total number of people' + str(df[df['PersonType']==persontype].shape[0])
  elif query_result.get('action') == 'find.email':
    emailcontain = query_result.get('parameters').get('email')
    fulfillmentText = 'Emails starting with ' + str(emailcontain) + str(df1[list(map(lambda x: x.startswith(emailcontain), df1['EmailAddress']))].EmailAddress)
  return {
        "fulfillmentText": fulfillmentText,
        "source": "webhookdata"
    }
   
if __name__ == '__main__':
  app.run(host='0.0.0.0', port=8080) # This line is required to run Flask on repl.it